'use strict';

/* Controllers */

angular.module('myApp.controllers', []).controller('AppCtrl', function ($scope, $routeParams, $compile, $http, $rootScope, $window, $location, $sce) {
    try {
        console.log("App ctrl called");
        console.log($routeParams.appid + " : " + $routeParams.pageid);
        $rootScope.appid = $routeParams.appid;
        $rootScope.pageid = $routeParams.pageid;
        var pageDefinition = function (appDefinition, pageId) {
            var pageDef;
            var i;
            for (i = 0; i < appDefinition.pages.length; i++) {
                var tempPage = appDefinition.pages[i]
                if (tempPage.pageid == pageId) {
                    //alert (tempApp.name);
                    pageDef = tempPage;
                    break;
                }
            }
            return pageDef;
        };
        $scope.init = function (app, appid1, pageid1) {
            console.log("Init called : " + appid1 + " : " + pageid1);
            console.log(app.title);
            if (app.appdef) {
                setTimeout(function () {

                    console.log(app.appdef.name);
                    var pagedef = app.appdef.pages[0];
                    if (appid1 == app.appdef.name) {
                        console.log("FOUND : " + appid1);
                        pagedef = pageDefinition(app.appdef, pageid1);
                    }
                    var appid = app.appdef.name;
                    $("#widget" + appid).html("");
                    console.log("page Def : " + pagedef);
                    var updatedprocessor = pagedef.pageprocessor.split("#appContent").join("#widget" + appid);
                    window.eval(updatedprocessor);
                    var pageProcessorName = 'pageprocessor' + pagedef.pageid;

                    if (window[pageProcessorName] != undefined) {
                        window[pageProcessorName](pagedef, $scope, $routeParams, $compile,
                            $http, $rootScope, $sce, $window, $location);
                        $scope.$apply();
                    }
                }, 100);
            }
        }
    } catch (ex) {
        console.log(ex);
    }

}).controller('HomeCtrl', function ($rootScope, $scope, $http, $location, $window, $route, $compile) {
    try {

        console.log("home called");
        $scope.selectedYear = '';
        $scope.selectedBranch = '';
        $scope.selectedClass = '';
        $scope.downloadlink = '';
        var user = {};


        $http.get("/kryptosattendance/attendance/userDetail").then(function (data) {
            console.log('user Detail data' + JSON.stringify(data));
            user = data.data;

            $http.post("/kryptosattendance/attendance/getYear", {tenant: user.tenant}).success(function (data) {
                console.log(data);
                $rootScope.years = data.year;
            }).error(function (data) {
                console.log("Error " + data);
            });

        }, function () {
            console.log("Error ");
        });


        $scope.getTime = function (val) {
            return parseInt(val / 60) + ":" + val % 60;
        }

        $scope.selectYear = function () {
            $scope.downloadlink = '';
            $rootScope.roster = '';
            //alert($scope.selectedYear);
            if ($scope.selectedYear != '') {
                $http.post("/kryptosattendance/attendance/getBranch", {tenant: user.tenant}).success(function (data) {
                    console.log(data);
                    $rootScope.branches = data.Branches;
                }).error(function (data) {
                    console.log("Error " + data);
                });
            }
        }


        $scope.selectBranch = function () {
            $scope.downloadlink = '';
            $rootScope.roster = '';
            //alert($scope.selectedBranch);
            if ($scope.selectedBranch != '') {
                $http.post("/kryptosattendance/attendance/getSubject", {
                    tenant: user.tenant,
                    branchId: $scope.selectedBranch
                }).success(function (data) {
                    console.log(data);
                    $rootScope.subjects = data.Subjects[0].subjects;
                }).error(function (data) {
                    console.log("Error " + data);
                });
            }
        }
        $scope.selectSubject = function () {
            $scope.downloadlink = '';
            $rootScope.roster = '';
            if ($scope.selectedBranch != '' && $scope.selectedSubject != '') {
                $http.post("/kryptosattendance/attendance/getAllclassRecords",
                    {
                        tenant: user.tenant,
                        branchId: $scope.selectedBranch,
                        subjectId: $scope.selectedSubject
                    }).success(function (data) {
                    console.log(data);
                    $rootScope.classes = data.class;
                }).error(function (data) {
                    console.log("Error " + data);
                });
            }
        }

        $scope.selectClass = function () {
            $scope.downloadlink = '';
            $rootScope.roster = '';
            if ($scope.selectedClass != '') {
                var sclass = $rootScope.classes[$scope.selectedClass];
                $scope.sclass = sclass;
                console.log(sclass);
                $scope.reporttitle = "Date/Time : " + sclass.date + " " + $scope.getTime(sclass.stTime) + " - " + $scope.getTime(sclass.endTime);
                $http.post("/kryptosattendance/attendance/getAttendanceRoster", sclass).success(function (data) {
                    console.log(data);

                    $rootScope.roster = data.roster;
                    $scope.totalCount = $rootScope.roster.length;
                    var pcount = 0;
                    var acount = 0;
                    $.each($rootScope.roster, function (i, val) {
                        if (val.status == 'present') {
                            pcount++;
                        } else {
                            acount++;
                        }
                    });
                    $scope.presentCount = pcount;
                    $scope.absentCount = acount;
                }).error(function (data) {
                    console.log("Error " + data);
                });

            }
        }

        $scope.downloadReport = function () {
            $scope.downloadlink = '';
            if ($scope.selectedClass != '') {
                var sclass = $rootScope.classes[$scope.selectedClass];
                $scope.sclass = sclass;
                console.log(sclass);
                $scope.reporttitle = "Date/Time : " + sclass.date + " " + $scope.getTime(sclass.stTime) + " - " + $scope.getTime(sclass.endTime);
                $http.post("/kryptosattendance/attendance/downloadReport", sclass).success(function (data) {
                    console.log(data);
                    $scope.downloadlink = data.url;
                    alert("Report generated successfully.");
                }).error(function (data) {
                    console.log("Error " + data);
                });
            }

        }
        $rootScope.trustHtml = function (html) {
            return $sce.trustAsHtml(html);
        }
        $BODY = $('body'),
            $MENU_TOGGLE = $('#menu_toggle'),
            $SIDEBAR_MENU = $('#sidebar-menu'),
            $SIDEBAR_FOOTER = $('.sidebar-footer'),
            $LEFT_COL = $('.left_col'),
            $RIGHT_COL = $('.right_col'),
            $NAV_MENU = $('.nav_menu'),
            $FOOTER = $('footer');
        var setContentHeight = function () {
            // reset height
            $RIGHT_COL.css('min-height', $(window).height());

            var bodyHeight = $BODY.outerHeight(),
                footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
                leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
                contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

            // normalize content
            contentHeight -= $NAV_MENU.height() + footerHeight;

            $RIGHT_COL.css('min-height', contentHeight);
        };

        $rootScope.sideClick = function ($event) {
            console.log("clicked " + $event.target);
            var $li = $($event.target).parent();
            console.log($li);
            if ($li.is('.active')) {
                $li.removeClass('active active-sm');
                $('ul:first', $li).slideUp(function () {
                    setContentHeight();
                });
            } else {
                // prevent closing menu if we are on child menu
                if (!$li.parent().is('.child_menu')) {
                    $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                    $SIDEBAR_MENU.find('li ul').slideUp();
                }

                $li.addClass('active');

                $('ul:first', $li).slideDown(function () {
                    setContentHeight();
                });
            }

        }
    } catch (ex) {
        console.log(ex);
    }

}).controller('DashboardCtrl', function ($rootScope, $scope, $routeParams, $http, $location, $window, $route, $compile) {
    // write Ctrl here
    console.log("DashboardCtrl called compile");
    var dbid = $routeParams.dbid;
    var dboard = null;
    console.log("dashboard");
    $http.get("/api/metadata/db").success(function (data) {
        console.log(data);
        $rootScope.dbdata = data;
    }).error(function (data) {
        console.log("Error " + data);
    });
}).controller('AdminCtrl', function ($rootScope, $scope, $routeParams, $http, $location, $window, $route, $compile) {
    // write Ctrl here
    console.log("AdminCtrl called compile");
    $scope.selectApp = function () {
        alert("called  " + $scope.seletedapp);
        $http.get("/api/kryptos/livedata/" + $scope.seletedapp).success(function (data) {
            console.log(data);
            $scope.selectedappdata = data;
        }).error(function (data) {
            console.log("Error " + data);
        });

    }

    $http.get("/api/kryptos/listMyApps").success(function (data) {
        console.log(data);
        $scope.myapps = data;
    }).error(function (data) {
        console.log("Error " + data);
    });
});
