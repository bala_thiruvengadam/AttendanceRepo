var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var expressValidator = require('express-validator');
var cors = require('cors');
var config = require("./config/config");

var routes = require('./routes/index');
var flash = require('connect-flash');
var users = require('./routes/users');
var dashboard = require('./routes/dashboard');
var session = require('express-session');
var schedule = require('node-schedule');
var request = require('request');
var passport = require('passport');

var profile=require('./routes/profile');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/AttendanceLatest');
var db = mongoose.connection;





var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
/*app.set('view engine', 'ejs');*/
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '5mb'}));

app.use(bodyParser.urlencoded({limit: '5mb', extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(require('express-session')({ secret: 'keyboard cat', resave: false, saveUninitialized: false, cookie: { maxAge: 3600000 } }));
app.use("/kryptosattendance",express.static(path.join(__dirname, 'public')));
app.use(cors());

//Express validator
app.use(expressValidator({
    errorFormatter: function(param, msg, value) {
        var namespace = param.split('.')
            , root    = namespace.shift()
            , formParam = root;

        while(namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param : formParam,
            msg   : msg,
            value : value
        };
    }
}));


//Express Session
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true

}));


//Passport init
app.use(passport.initialize());
app.use(passport.session());

//connect flash
app.use(flash());

// Global Vars
app.use(function (req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});


var ips = ['127.0.0.1', '10.2.5.18'];

// Allow only whitelisted ips.
//
//app.use(ipfilter(ips,{mode:'allow'}));
app.use(function(req, res, next){
  res.locals.api = config.api;
  next();
});

app.use('/kryptosattendance/dashboard', dashboard);
app.use('/kryptosattendance/dashboard/partials/:name',  require("./routes/dashboard"));

// app.use('/users', users);
//require("./routes/api")(app);
app.use('/kryptosattendance', require("./routes/api"));

app.use('/profile', profile);


var j = schedule.scheduleJob('*/5 * * * *', function(){
  console.log('cron job called');
    request.post('http://localhost:3000/kryptosattendance/attendance/deactivateClass', { form: { tenant:'laxmibai' }}, function (error, response) {
      //console.log(response.body);console.log(error);
    });
});



/*app.use('/kryptosattendance/dashboard', require('connect-ensure-login').ensureLoggedIn(), require("./routes/dashboard"));*/
/*app.use('/kryptosattendance/dashboard/login', require("./routes/dashboard"));*/
/*app.get('/kryptosattendance/dashboard/login', function(req, res, next) {
  res.render('login', { title: 'Express' });
});
app.post('/kryptosattendance/dashboard/login',
  passport.authenticate('local', { failureRedirect: '/kryptosattendance/dashboard/login' }),
  function(req, res) {
    res.redirect('/kryptosattendance/dashboard');
  });
app.get('/kryptosattendance/dashboard/logout',
  function(req, res){
    req.logout();
    res.redirect('/kryptosattendance/dashboard');
});
*/
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
