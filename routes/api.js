var express = require('express');
var compareJSON = require('json-structure-validator');

var router = express.Router();
var _ = require('lodash');
var metaDataStructure = {
    "clientId": "",
    "appId": "",
    "featureId": "",
    "subFeature": "",
    "metaData": ""
}

var metaDataStructureWithId = _.extend({"_id": ""}, metaDataStructure);

/**
 * To Validate the structure of the JSON file being sent from the client side.
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
var canProcessMetaDataPost = function (req, res, next) {
    var comparison = compareJSON(req.body, metaDataStructure);
    var result = "Valid Request"
    if (comparison != true) {
        comparison = compareJSON(req.body, metaDataStructureWithId);
    }
    if (comparison != true) {
        result = comparison;
        res.status(401).send(result);
    } else {
        next();
    }
}


var attendanceController = require("../routes/controller/attendanceController");
var qrController = require("../routes/controller/qrCodeController");
var facultyController = require("../routes/controller/facultyController");
var branchController = require("../routes/controller/branchController");
var branchSubjectController = require("../routes/controller/branchSubjectController");
var activateClassController = require("../routes/controller/activateClassController");
var manualAttendanceController = require("../routes/controller/manualAttendanceController");
var YearSemesterController = require("../routes/controller/YearSemesterController");
var UserDetailsController = require("../routes/controller/UserDetailsController")

/************Qr code api's end point *****************/

router.route("/attendance/addQrCode")
    .post(qrController.addQrCode);

router.route("/attendance/getQrCode")
    .post(qrController.getQrCode);

router.route("/attendance/getAvailableQrCodes")
    .post(qrController.getAvailableQrCodes);

/************End of Qr code api's end point **********/


/************Faculty api's end point *****************/

router.route("/attendance/addFaculty")
    .post(facultyController.addFaculty);

router.route("/attendance/getFaculty")
    .post(facultyController.getFaculty);

/************End of faculty api's end point **********/


/************Branch api's end point *****************/

router.route("/attendance/addYear")
    .post(YearSemesterController.addYear);

router.route("/attendance/getYear")
    .post(YearSemesterController.getYear);


router.route("/attendance/addBranch")
    .post(branchController.addBranch);

router.route("/attendance/getBranch")
    .post(branchController.getBranch);

/************End of Branch api's end point **********/

/************Subject api's end point *****************/

router.route("/attendance/addSubject")
    .post(branchSubjectController.addSubject);

router.route("/attendance/getSubject")
    .post(branchSubjectController.getSubject);

/************End of Subject api's end point **********/

/************activated class api's end point *****************/

router.route("/attendance/activateClass")
    .post(activateClassController.activateClass);
router.route("/attendance/classRecords")
    .post(activateClassController.classRecords);
router.route("/attendance/getclassRecords")
    .post(activateClassController.getclassRecords);
router.route("/attendance/updateClassRecords")
    .post(activateClassController.updateClassRecords);
router.route("/attendance/checkSubmitClassRecord")
    .post(activateClassController.checkSubmitClassRecord);
router.route("/attendance/checkClassActivated")
    .post(activateClassController.checkClassActivated);

router.route("/attendance/getActiveQRCodes")
    .post(activateClassController.getActiveQRCodes);

router.route("/attendance/getClassDetail")
    .post(activateClassController.getClassDetail);

router.route("/attendance/deactivateClass")
    .post(activateClassController.deactivateClass);

router.route("/attendance/deactivateClassManually")
    .post(activateClassController.deactivateClassManually);

/************End of activated class api's end point **********/


/************Student details api's end point *****************/

router.route("/attendance/getClassStudents")
    .post(manualAttendanceController.getClassStudents);
router.route("/attendance/timezoneapi")
    .post(manualAttendanceController.timezoneapi);
    

/************End of Student details api's end point **********/

/************mark attendance api's end point *****************/

router.route("/attendance/markUserAttendance")
    .post(attendanceController.markUserAttendance);
router.route("/attendance/unMarkUserAttendance")
    .post(attendanceController.unMarkUserAttendance);

router.route("/attendance/getMarkedStudents")
    .post(attendanceController.getMarkedStudents);
router.route("/attendance/UserAttendanceSubmit")
    .post(attendanceController.UserAttendanceSubmit);
router.route("/attendance/AddOnUserAttendanceSubmit")
    .post(attendanceController.AddOnUserAttendanceSubmit);
router.route("/attendance/AddOnCancelClass")
    .post(attendanceController.AddOnCancelClass);
router.route("/attendance/UserCancelClass")
    .post(attendanceController.UserCancelClass);
router.route("/attendance/submitClassesRecord")
    .post(attendanceController.submitClassesRecord);
router.route("/attendance/checkSubmitClassesRecord")
    .post(attendanceController.checkSubmitClassesRecord);
router.route("/attendance/cancelClassesRecord")
    .post(attendanceController.cancelClassesRecord);
router.route("/attendance/testAsync")
    .post(attendanceController.testAsync);
router.route("/attendance/AddOnUserAttendanceAllPresent")
    .post(attendanceController.AddOnUserAttendanceAllPresent);

router.route("/attendance/UserAttendanceAllPresent")
    .post(attendanceController.UserAttendanceAllPresent);

    
router.route("/attendance/createExcel")
    .post(attendanceController.createExcel);

router.route("/attendance/getAllclassRecords")
    .post(activateClassController.getAllclassRecords);

router.route("/attendance/getAttendanceRoster")
    .post(attendanceController.getAttendanceRoster);

router.route("/attendance/downloadReport")
    .post(attendanceController.downloadReport);

router.route("/attendance/AddDetails")
    .post(activateClassController.AddDetails);



/************mark attendance api's end point **********/

router.route("/attendance/userDetail")
    .get(UserDetailsController.userDetail);

    
module.exports = router;